namespace BetaOnline.Services
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using System.Net;
	using System.Xml;
	using System.IO;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SxGetCallUrlService
    {
        [OperationContract]
		[WebGet(UriTemplate = "GetUrl/{uniqueId}", ResponseFormat = WebMessageFormat.Json)] 
        public string GetUrl(string uniqueId)
        {
            WebClient client = new WebClient();
            uniqueId = uniqueId.Replace('_', '.');

            Stream data = client.OpenRead("https://#############################?unique_id=" + uniqueId);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();
            var result = (JObject)JsonConvert.DeserializeObject(s);
            if (result["#######"].Count() > 0) {
            	return result["#######"].0].ToString();
            }
            else {
            	return "";
            }
        }
    }
}
