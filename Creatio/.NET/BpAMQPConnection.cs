namespace BetaOnline.ExternalConnections
{
    using System.Collections.Generic;
    using System;
    using BetaOnline.ExternalConnections.Interfaces;
    using System.Text;
    using Terrasoft.Core;
    using Terrasoft.Core.Entities;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;

    /// <summary>
    /// AMQP connection.
    /// </summary>
    public class BpAMQPConnection : BpExternalConnectionFactory, IDisposable
    {
        #region Properties: Private

        /// <summary>
        /// Timeout after which the channel will be closed if there is no confirmation.
        /// </summary>
        private const int CONFIRM_TIMEOUT = 5;

        /// <summary>
        /// Instance of connection.
        /// </summary>
        private IConnection _connection;

        /// <summary>
        /// Instance of channel.
        /// </summary>
        private IModel _channel = null;

        /// <summary>
        /// Instance of connection factory.
        /// </summary>
        private ConnectionFactory _factory = null;

        /// <summary>
        /// Displayed connection name.
        /// </summary>
        private string _connectionName;

        /// <summary>
        /// Exchange name.
        /// </summary>
        private string _exchange = "";

        #endregion

        #region Properties: Public

        #endregion

        #region Methods: Private

        /// <summary>
        /// Initialize connection parameters.
        /// </summary>
        private void Initialize()
        {
            Entity externalConnectionData = ExternalConnectionEntity();

            string hostName = externalConnectionData.GetTypedColumnValue<string>("BpURL");
            string userName = externalConnectionData.GetTypedColumnValue<string>("BpLogin");
            string password = externalConnectionData.GetTypedColumnValue<string>("BpPassword");
            _connectionName = externalConnectionData.GetTypedColumnValue<string>("Name");

            EntityCollection entities = ExternalConnectionParameters();

            _parameters = new Dictionary<string, string>();
            foreach (Entity entity in entities)
            {
                _parameters.Add(entity.GetTypedColumnValue<string>("BpParameter"), entity.GetTypedColumnValue<string>("BpValue"));
            }

            _exchange = _parameters["Exchange"];

            _factory = new ConnectionFactory()
            {
                UserName = userName,
                Password = password,
                HostName = hostName
            };
            if (_parameters.ContainsKey("VirtualHost"))
                _factory.VirtualHost = _parameters["VirtualHost"];
        }

        /// <summary>
        /// Callback method if server doesnt send ack.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChannelBasicReturn(object sender, BasicReturnEventArgs e)
        {
            throw new Exception("BasicReturn: " + e.ReplyText);
        }

        #endregion

        #region Methods: Public

        /// <summary>
        /// Create instance of AMQP connection object and initialize parameters.
        /// </summary>
        /// <param name="externalConnectionId"> External connection Id. </param>
        /// <param name="userConnection"> User connection. </param>
        public BpAMQPConnection(Guid externalConnectionId, UserConnection userConnection) : base(externalConnectionId, userConnection)
        {
            Initialize();
        }

        /// <summary>
        /// Create AMQP request with given exchange and routing key.
        /// </summary>
        /// <param name="exchange"> Exchange name. </param>
        /// <param name="routingKey"> Routing key. </param>
        /// <returns> Instance of AMQP request. </returns>
        public BpAMQPMessage CreateRequest(string exchange = "", string routingKey = "")
        {
            if (string.IsNullOrWhiteSpace(exchange))
                exchange = _parameters["Exchange"];

            if (string.IsNullOrWhiteSpace(routingKey))
                routingKey = _parameters["RoutingKey"];

            BpAMQPMessage request = new BpAMQPMessage(exchange, routingKey);

            return request;
        }
        
        /// <summary>
        /// Create AMQP request instance with exhange and routing key from parameters.
        /// </summary>
        /// <returns> Instance of AMQP request. </returns>
        public override BpExternalConnectionRequest CreateRequest()
        {
            return CreateRequest(string.Empty, string.Empty);
        }

        /// <summary>
        /// Send AMQP message with confirm.
        /// </summary>
        /// <param name="request"> AMQP request. </param>
        public override IExternalConnectionResponse Send(BpExternalConnectionRequest request)
        {
            BpAMQPMessage message = (BpAMQPMessage)request;

            var publishProperties = _channel.CreateBasicProperties();

            publishProperties.Persistent = true;
            long timestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            publishProperties.Timestamp = new AmqpTimestamp(timestamp);

            publishProperties.MessageId = message.MessageId;
            publishProperties.Priority = message.Priority;
            publishProperties.CorrelationId = message.CorrelationId;
            publishProperties.Type = message.MessageType;
            
            publishProperties.Headers = message.Headers;

            var body = Encoding.UTF8.GetBytes(message.Body);

            _channel.ConfirmSelect();
            _channel.BasicPublish(exchange: message.Exchange,
                                 routingKey: message.RoutingKey,
                                 mandatory: true,
                                 basicProperties: publishProperties,
                                 body: body);

            _channel.WaitForConfirmsOrDie(TimeSpan.FromSeconds(CONFIRM_TIMEOUT));

            return null;
        }

        /// <summary>
        /// Declare new queue and bind to direct exchange by routing key.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        /// <param name="exchangeName"> Existing exhcnage name. </param>
        /// <param name="routingKey"> Routing key for binding. </param>
        /// <param name="maxPriority"> Set max message priority in queue. </param>
        public void DeclareAndBindQueue(string queueName, string exchangeName, string routingKey, int maxPriority = 0)
        {
            Dictionary<string, object> queueArguments = new Dictionary<string, object>
            {
                { "x-max-priority", maxPriority }
            };

            _channel.ConfirmSelect();
            _channel.QueueDeclare(queue: queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: queueArguments);
            _channel.WaitForConfirmsOrDie(TimeSpan.FromSeconds(CONFIRM_TIMEOUT));

            _channel.ConfirmSelect();
            _channel.QueueBind(queueName, exchangeName, routingKey, null);
            _channel.WaitForConfirmsOrDie(TimeSpan.FromSeconds(CONFIRM_TIMEOUT));
        }

        /// <summary>
        /// Delete queue by name.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        public void DeleteQueue(string queueName)
        {
            _channel.ConfirmSelect();
            _channel.QueueDelete(queueName);
            _channel.WaitForConfirmsOrDie(TimeSpan.FromSeconds(CONFIRM_TIMEOUT));
        }

        /// <summary>
        /// Get the number of messages in Ready state in the queue.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        /// <returns> Return -1 if queue didn't declare. </returns>
        public int GetMessagesCount(string queueName)
        {
            if (IsQueueExist(queueName))
            {
                QueueDeclareOk response = _channel.QueueDeclarePassive(queueName);
                return Convert.ToInt32(response.MessageCount);
            }

            return -1;
        }

        /// <summary>
        /// Check is queue exist.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        /// <returns></returns>
        public bool IsQueueExist(string queueName)
        {
            IModel tempChannel = _connection.CreateModel();

            QueueDeclareOk response;
            try
            {
                response = tempChannel.QueueDeclarePassive(queueName);
            }
            catch (Exception)
            {
                tempChannel.Close();
                return false;
            }

            tempChannel.Close();
            return true;
        }

        /// <summary>
        /// Get one message from queue.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        /// <returns></returns>
        public string GetMessage(string queueName)
        {
            bool noAck = false;
            BasicGetResult result = _channel.BasicGet(queueName, noAck);
            if (result == null)
            {
                // No messages available
                return "";
            }
            _channel.BasicAck(result.DeliveryTag, false);

            byte[] body = result.Body.ToArray();
            string originalMessage = Encoding.UTF8.GetString(body);

            return originalMessage;
        }

        /// <summary>
        /// Get several messages from queue.
        /// </summary>
        /// <param name="queueName"> Queue name. </param>
        /// <param name="count"> Messages count. </param>
        /// <returns></returns>
        public List<string> GetMessages(string queueName, int count)
        {
            List<string> messages = new List<string>();
            if (count < 1)
                return messages;

            for (int i = 0; i < count; i++)
            {
                string message = GetMessage(queueName);
                if (!string.IsNullOrWhiteSpace(message))
                    messages.Add(message);
            }

            return messages;
        }

        /// <summary>
        /// Retrun AMQP external connection exchange name.
        /// </summary>
        /// <returns></returns>
        public string GetExchangeName()
        {
            return _exchange;
        }

        /// <summary>
        /// Open channel.
        /// </summary>
        public void OpenChannel()
        {
            _channel = _connection.CreateModel();
            _channel.BasicReturn += ChannelBasicReturn;
        }

        /// <summary>
        /// Close channel.
        /// </summary>
        public void CloseChannel()
        {
            _channel.Close();
        }

        /// <summary>
        /// Create connection.
        /// </summary>
        public void CreateConnection()
        {
            _connection = _factory.CreateConnection(_connectionName);
        }

        /// <summary>
        /// Create connection with name.
        /// </summary>
        /// <param name="name"> Connection name. </param>
        public void CreateConnection(string name)
        {
            _connection = _factory.CreateConnection($"{ _connectionName }: { name }");
        }

        /// <summary>
        /// Close connection.
        /// </summary>
        public void CloseConnection()
        {
            _connection.Close();
        }

        /// <summary>
        /// Close channel and connection.
        /// </summary>
        public void Close()
        {
            if (_connection == null)
                return;

            if (_connection.IsOpen)
                _connection.Close();

            try {
                _connection.Dispose();
            }
            catch (Exception ex)
            {
                BetaOnline.Logger.BpDbLogger.AddLog(_userConnection, ex.Message, System.Diagnostics.EventLogEntryType.Error, ExternalConnectionId.ToString(), true);
            }

            _connection = null;
        }

        public void Dispose()
        {
            Close();
        }
        
        #endregion
    }
}
