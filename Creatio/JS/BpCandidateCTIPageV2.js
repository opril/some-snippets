define("BpCandidateCTIPageV2", ["BpClientConstants", "BpBetaPressConstants",
"BpDialogScenarioPopupMixin"],
	function(BpClientConstants, BetaPressConstants) {
		return {
			entitySchemaName: "SxVacancyCandidate",
			
			mixins: {
				DialogScenarioPopupMixin: "Terrasoft.BpDialogScenarioPopupMixin"
			},
			
			modules: /**SCHEMA_MODULES*/{
				"ActionsDashboardModule": {
					"config": {
						"isSchemaConfigInitialized": true,
						"schemaName": "SectionActionsDashboard",
						"useHistoryState": false,
						"parameters": {
							"viewModelConfig": {
								"entitySchemaName": "SxVacancyCandidate",
								"actionsConfig": {
									"schemaName": "BpCandidateStatus",
									"columnName": "BpStatus",
									"filterColumnName": "BpShowInCTIDashboard",
									"orderColumnName": "BpCTIDashboardPosition",
									"innerOrderColumnName": "BpRank",
									"colorColumnName": "BpCategory.BpColor"
								},
								"useDashboard": true,
								"contentVisible": true,
								"headerVisible": false,
								"dashboardConfig": {
									"Activity": {
										"masterColumnName": "Id",
										"referenceColumnName": "SxVacancyCandidate"
									}
								}
							}
						}
					}
				},
				"CandidateParametersModule": {
					moduleId: "CTIPage_CandidateParametersModule",
					moduleName: "BpCandidateParametersSchemaModule",
					config: {
						parameters: { 
							// Параметры, передаваемые в схему при ее инициализации.
							viewModelConfig: { 
								HeaderEnabled: true,
								SettingsEnabled: false,
								RelevancePassEnabled: false,
								IsEditable: true
							}
						}
					}
				},
				"FilterModule": {
					moduleId: "CTIPage_FilterEditModule",
					moduleName: "FilterEditModule",
					config: {}
				}
			}/**SCHEMA_MODULES*/,
			
			details: {
				"VacancyRequirement": {
					schemaName: "SxVacancyRequirementDetail",
					entitySchemaName: "SxVacancyRequirement",
					filter: {
						masterColumn: "SxVacancy",
						detailColumn: "SxVacancy"
					}
				},
				"CandidateAddress": {
					schemaName: "SxCandidateAddressDetailV2",
					filter: {
						masterColumn: "CandidateId",
						detailColumn: "SxCandidate"
					}
				},
				"BpVacancyCandidateFailureStatisticsDetail": {
					schemaName: "BpVacancyCandidateFailureStatisticsDetail",
					entitySchemaName: "BpVacancyCandidateFailureStatistics",
					filter: {
						detailColumn: "BpVacancyCandidate",
						masterColumn: "Id"
					}
				}
			},

			messages: {
				"CandidateCTIPageIsOpen": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				},

				"GetCTICandidateId": {
					"mode": Terrasoft.MessageMode.PTP,
                    "direction": Terrasoft.MessageDirectionType.PUBLISH
				},

				"VacancySelected": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				},

				"GetIsCandidateExist": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},

				"GetCandidateId": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				"UpdateCandidateParametersModule": {
					"mode": Terrasoft.MessageMode.PTP,
					"direction": Terrasoft.MessageDirectionType.PUBLISH
				},

				"CreateDialogScenarioPopup": {
					"mode": Terrasoft.MessageMode.PTP,
					"direction": Terrasoft.MessageDirectionType.PUBLISH
				},

				"UpdateCandidateParametersModuleSettings": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				"UpdateRelevancePassInformation": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				},
				"GetRelevancePassModuleConfig": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				"RelevancePassResult": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},

				"GetFilterModuleConfig": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
	
				"SetFilterModuleConfig": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			
			attributes: {
				//#region Entity columns
				"SxVacancy": {
					type: this.Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					isRequired: true,
					lookupListConfig: {
						columns: ["SxName", "SxOpportunity", "SxPhone", "SxOpportunity.Account", "BpRequirementFilterValue",
							"SxAgeFrom", "SxAgeTill", "SxCallScript", "SxOpportunity.BpUseNewRequirement",	
							"SxOpportunity.Title", "SxOpportunity.Account.Name", "SxOpportunity.SxPlanningEndDate"],
						actionsButtonVisible: false,
						filters: [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;

								// Только вакансии в работе
								filterGroup.add("statusVacancyFilter", Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"SxVacancyStage",
									BpClientConstants.Vacancy.Stage.InWork
								));

								// Administrator and manager see all vacancies. 
								if (this.$OperatorRoleId !== BpClientConstants.Contact.DecisionRole.Administrator
									&& this.$OperatorRoleId !== BpClientConstants.Contact.DecisionRole.Manager) {

									filterGroup.add("filterVacancyByOperator", Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"[SxVacancyOperator:SxVacancy].SxOperator.Id",
										Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
								}

								return filterGroup;
							}
						]
					},
					dependencies: [
						{
							columns: ["SxVacancy"],
							methodName: "onVacancyChanging"
						}
					]
				},

				"SxWorkplaceAddress": {
					type: this.Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						columns: ["City.Name", "Address", "SxSubwayStation.Name"],
						actionsButtonVisible: false,
						filter: function() {
							var vacancy = this.get("SxVacancy");
							if (!Ext.isEmpty(vacancy)) {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								
								var opportunityFilter = Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL, "Account", vacancy["SxOpportunity.Account"].value);
								var interviewAddressTypeIds = [
									BpClientConstants.AccountAddress.Type.Workplace,
									BpClientConstants.AccountAddress.Type.WorkplaceAndInterview
								];
								var addressTypeFilter = Terrasoft.createColumnInFilterWithParameters("AddressType", interviewAddressTypeIds);
								
								filterGroup.add("opportunityFilter", opportunityFilter);
								filterGroup.add("addressTypeFilter", addressTypeFilter);
								return filterGroup;
							}
						}
					},
					dependencies: [
						{
							columns: ["SxWorkplaceAddress"],
							methodName: "onWorkplaceAddressChanged"
						}
					]
				},

				"BpInterviewAddress": {
					type: Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						columns: ["City.Name", "Address", "SxSubwayStation.Name"],
						actionsButtonVisible: false,
						filter: function() {
							var vacancy = this.get("SxVacancy");
							if (!Ext.isEmpty(vacancy)) {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								
								var opportunityFilter = Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL, "Account", vacancy["SxOpportunity.Account"].value);
								var interviewAddressTypeIds = [
									BpClientConstants.AccountAddress.Type.Interview,
									BpClientConstants.AccountAddress.Type.WorkplaceAndInterview
								];
								var addressTypeFilter = Terrasoft.createColumnInFilterWithParameters("AddressType", interviewAddressTypeIds);
								
								filterGroup.add("opportunityFilter", opportunityFilter);
								filterGroup.add("addressTypeFilter", addressTypeFilter);
								return filterGroup;
							}
						}
					},
					dependencies: [
						{
							columns: ["BpInterviewAddress"],
							methodName: "onInterviewAddressChanged"
						}
					]
				},

				"BpStore": {
					type: Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						filter: function() {
							if (Ext.isEmpty(this.$SxVacancy)) {
								return;
							}
							let filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("filterStoreByVacancy",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"BpAccount.[BpOrder:BpAccount].[Opportunity:BpOrder].[SxVacancy:SxOpportunity].Id", this.$SxVacancy.value));

							return filterGroup;
						}
					}
				},

				"BpStatus": {
					type: Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						columns: ["BpIsDetailsRequired"]
					},
					dependencies: [
						{
							columns: ["BpStatus"],
							methodName: "onStatusChanging"
						}
					]
				},

				"BpStatusDetails": {
					type: Terrasoft.ViewModelColumnType.ENTITY_COLUMN,
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						filters: [
							function () {
								if (Ext.isEmpty(this.$BpStatus)) {
									return;
								}
								let filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.add("filterDetailsByStatus",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"[BpCandidateStatusToDetails:BpCandidateStatusDetails].BpCandidateStatus.Id", this.$BpStatus.value));

								return filterGroup;
							}
						]
					}
				},
				//#endregion Entity columns

				//#region Virtual columns
				"AccountOpportunityVacancyCaption": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"DaysBeforeClosingOpportunity": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"VacancyCandidateSaved": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},

				"VacancyCandidateId": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.GUID
				},

				"OperatorPhoneNumber": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"CandidateId": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.GUID
				},

				"FirstName": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"dependencies": [
						{
							columns: ["FirstName"],
							methodName: "onCandidateNameChanged"
						}
					]
				},

				"LastName": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"dependencies": [
						{
							columns: ["LastName"],
							methodName: "onCandidateNameChanged"
						}
					]
				},

				"MiddleName": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"MobilePhone": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"Email": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"Description": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"CareerObjective": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"initNumber": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"callUniqueId": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"callDirectionId": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"CallType": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				},

				"BpCTILog": {
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"value": ""
				},

				"OperatorRoleId": {
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: Terrasoft.DataValueType.GUID,
					value: null
				},

				"IsNewCandidate": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},

				"IsVacancyCandidateExist": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},

				"IsStatusDetailsRequired": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},

				"IsStatusDetailsVisible": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: false
				},

				"IsVacancyRequirementsV2Visible": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: false
				},

				"IsVacancyRequirementsVisible": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: false
				},

				"AgeFrom": {
					"dataValueType": this.Terrasoft.DataValueType.INTEGER,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"AgeTill": {
					"dataValueType": this.Terrasoft.DataValueType.INTEGER,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				//#endregion Virtual columns
			},
			
			methods: {
				init: function() {
					document.ctiV2 = this;

					this.set("Operation", Terrasoft.ConfigurationEnums.CardOperation.EDIT);
					this.callParent(arguments);
				},

				onEntityInitialized: function() {
					console.log("onEntityInitialized");
					this.showBodyMask({
						uniqueMaskId: "set-candidate-mask"
					});

					this.$CandidateId = this.sandbox.publish("GetCTICandidateId");
					
					this.getCandidate();

					let numbers = this.sandbox.publish("CandidateCTIPageIsOpen");
					if (numbers) {
						this.set("callUniqueId", numbers.callUniqueId);
						this.set("callDirectionId", numbers.typeId);
						this.set("initNumber", numbers.initNumber);
						this.set("MobilePhone", numbers.secondNumber);
						this.set("OperatorPhoneNumber", numbers.operatorNumber);
						this.set("CallType", numbers.type);
					}

					this.callParent(arguments);

					if (Ext.isEmpty(this.$Id)) {
						this.$Id = Terrasoft.generateGUID();
					}

					this.setOperatorRoleId();
					this.setDefValues();
					this.setCallSourceByInitNumber();
					this.setOpportunityAndVacancyByInitNumber();
				},

				subscribeSandboxEvents: function() {
					// Override message handler from actions dashboard.
					// Disable autosave card after status change.
					this.sandbox.subscribe("SaveRecord", function() {
						console.log("status changed.");
					}, this, [this.getModuleId("ActionsDashboardModule")]);

					this.callParent(arguments);

					// Relevance module config.
					// Return data (candidate Id and vacancy Id) for relevance check.
					this.sandbox.subscribe(
						"GetRelevancePassModuleConfig",
						function() {
							console.log("GetRelevancePassModuleConfig");
							return {
								CandidateId: this.$CandidateId,
								VacancyId: this.$SxVacancy ? this.$SxVacancy.value : null
							};
						}, this, []
					);

					// Relevance pass information.
					// Set status by relevance pass result.
					this.sandbox.subscribe("RelevancePassResult", function(result) {
						console.log("result pass", result);
						switch(result.Result) {
							case true:
								this.loadLookupDisplayValue("BpStatus", BpClientConstants.VacancyCandidate.Status.Passed);
								break;
							case false:
								this.loadLookupDisplayValue("BpStatus", BpClientConstants.VacancyCandidate.Status.Failed);
								break;
							default:
								this.loadLookupDisplayValue("BpStatus", BpClientConstants.VacancyCandidate.Status.Unknown);
								break;
						}
					}, this, []);

					// Candidate parameters module init.
					// Return current candidate Id.
					this.sandbox.subscribe(
						"GetCandidateId",
						function() {
							return this.$CandidateId;
						}, this, []
					);
					
					// Candidate parameters module update config.
					// Return relevance pass enabled.
					this.sandbox.subscribe(
						"UpdateCandidateParametersModuleSettings",
						function() {
							return {
								RelevancePassEnabled: this.$IsVacancyRequirementsV2Visible
							};
						}, this, []
					);

					// Filter module edit config for vacancy requirements.
					// Return module readonly config.
					let filerModuleId = this.modules.FilterModule.moduleId;
					this.sandbox.subscribe("GetFilterModuleConfig", function() {
						console.log("GetFilterModuleConfig");
						return this.getRequirementsModuleConfig();
					}, this, [filerModuleId]);

					// Candidate addresses and interview details adding record.
					// Returs true if candidate exist.
					this.sandbox.subscribe("GetIsCandidateExist", function() {
						return !this.$IsNewCandidate;
					}, this, ["CandidateAddressCTIDetail", "InterviewCTIDetail"]);
				},

				// If candidate exist load candidate data.
				// If candidate not exist create new one.
				getCandidate: function() {
					if (!Terrasoft.isGUID(this.$CandidateId)) {
						this.$CandidateId = Terrasoft.generateGUID();
					}
					let candidateId = this.$CandidateId;

					let esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "SxCandidate"
					});
					esq.addColumn("Id");
					esq.addColumn("SxName");
					esq.addColumn("SxMobilePhone");
					esq.addColumn("SxEmail");
					esq.addColumn("BpDescription");
					esq.addColumn("BpCareerObjective");
					esq.getEntity(candidateId, function(result) {
						console.log(result);
						if (result.success && result.entity) {
							this.setCandidateName(result.entity.get("SxName"));
							this.$MobilePhone = result.entity.get("SxMobilePhone");
							this.$Email = result.entity.get("SxEmail");
							this.$Description = result.entity.get("BpDescription");
							this.$CareerObjective = result.entity.get("BpCareerObjective");

							this.$IsNewCandidate = false;

							this.loadLookupDisplayValue("SxCandidate", this.$CandidateId);
							this.updateCandidateParametersModule();

							this.logEvent("Кандидат загружен.");
							this.hideBodyMask({
								uniqueMaskId: "set-candidate-mask"
							});
						} else {
							this.$IsNewCandidate = true;
							this.logEvent("Кандидат не найден в системе. Введите ФИО для создания.")
							this.hideBodyMask({
								uniqueMaskId: "set-candidate-mask"
							});
						}
					}, this);
				},

				// Split candidate full name and set.
				setCandidateName: function(name) {
					console.log("setCandidateName");
					let fullName = name.split(" ");

					if (fullName.length > 0) {
						this.$LastName = fullName[0];
						this.$FirstName = fullName[1];
						this.$MiddleName = fullName[2];
					}
				},

				// Create candidate, if it not created yet and first name or last name filled.
				onCandidateNameChanged: function() {
					if (this.$IsNewCandidate && (this.$LastName || this.$FirstName)) {
						this.saveCandidate(true);
					} else {
						this.updateCandidateParametersModule();
					}
				},

				// Update or create candidate if he do not exist.
				saveCandidate: function(showBodyMask, callback) {
					if (showBodyMask) {
						let maskId = this.showBodyMask({
							uniqueMaskId: "save-candidate-mask"
						});
						Terrasoft.MaskHelper.UpdateBodyMaskCaption(maskId, "Сохранение Кандидата");
					}

					var query;
					if (this.$IsNewCandidate) {
						query = Ext.create("Terrasoft.InsertQuery", {
							rootSchemaName: "SxCandidate"
						});
						query.setParameterValue("Id", this.$CandidateId, Terrasoft.DataValueType.GUID);
					} else {
						query = Ext.create("Terrasoft.UpdateQuery", {
							rootSchemaName: "SxCandidate"
						});
						query.filters.add("idFilter", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "Id", this.$CandidateId));
					}

					query.setParameterValue("BpFirstName", this.$FirstName, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("BpLastName", this.$LastName, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("BpMiddleName", this.$MiddleName, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("SxMobilePhone", this.$MobilePhone, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("SxEmail", this.$Email, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("BpDescription", this.$Description, Terrasoft.DataValueType.TEXT);
					query.setParameterValue("BpCareerObjective", this.$CareerObjective, Terrasoft.DataValueType.TEXT);

					query.execute(function(response) {
						if (response.success) {
							this.$IsNewCandidate = false;
							if (query.$className.indexOf("UpdateQuery") !== -1) {
								this.logEvent("Кандидат сохранен.");
							} else {
								this.logEvent("Кандидат создан.");
							}

							this.loadLookupDisplayValue("SxCandidate", this.$CandidateId);

							if (showBodyMask) {
								this.hideBodyMask({
									uniqueMaskId: "save-candidate-mask"
								});
							}

							if (callback) {
								callback.call(this);
							}
						}
					}, this);
				},

				save: function() {
					if (this.$IsNewCandidate) {
						this.showInformationDialog("Не удалось сохранить. Введите ФИО кандидата.");
						this.logEvent("Не удалось сохранить. Введите ФИО кандидата.");
						return;
					}

					if (this.$IsVacancyCandidateExist) {
						this.showExistVacancyCandidateConfirmationDialog();
						this.logEvent("Не удалось сохранить. Кандидат уже был добавлен в эту вакансию.");
						return;
					}

					if (Ext.isEmpty(this.$SxVacancy)) {
						this.showInformationDialog("Укажите вакансию.");
						this.logEvent("Не удалось сохранить. Укажите вакансию.");
						return;
					}

					if (Ext.isEmpty(this.$BpCallSource)) {
						this.showInformationDialog("Укажите источник звонка.");
						this.logEvent("Не удалось сохранить. Укажите источник звонка.");
						return;
					}

					if (this.$IsStatusDetailsRequired && Ext.isEmpty(this.$BpStatusDetails)) {
						this.showInformationDialog("Укажите причину отказа");
						this.logEvent("Не удалось сохранить. Укажите причину отказа.");
						return;
					}

					this.saveCandidate();

					let maskId = this.showBodyMask({
						uniqueMaskId: "save-vacancy-candidate-mask"
					});
					Terrasoft.MaskHelper.UpdateBodyMaskCaption(maskId, "Сохранение КВВ");

					this.$BpInitCallUniqueId = this.$callUniqueId;
					this.$BpCallCount = 1;
					this.loadLookupDisplayValue("BpInitCallDirection", this.$callDirectionId);
					this.loadLookupDisplayValue("BpOrigin", BetaPressConstants.candidate.origin.operator);
					this.loadLookupDisplayValue("SxCandidate", this.$CandidateId);

					this.callParent({isSilent: true});
				},
				

				onSaved: function() {
					this.callParent(arguments);

					this.hideBodyMask({
						uniqueMaskId: "save-vacancy-candidate-mask"
					});

					this.$VacancyCandidateSaved = true;

					this.logEvent("КВВ сохранен.");

					// Add candidate to call.
					if (!Ext.isEmpty(this.$CandidateId) && !Ext.isEmpty(this.$callUniqueId)) {
						let curCall = this.$callUniqueId;
						let update = this.Ext.create("Terrasoft.UpdateQuery", {
							rootSchemaName: "Call"
						});
						update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "IntegrationId", curCall));
						update.setParameterValue("SxCandidate", this.$CandidateId, this.Terrasoft.DataValueType.GUID);
						update.execute(function(response) {
							if (response.success) {
								this.logEvent("Звонок обновлен. Добавлен Кандидат.");
							}
						}, this);
					}

					this.tryUpdateVacancyCandidateInCall(this.$Id, this.$callUniqueId, 0);
				},

				// Create message in temp log for this page.
				logEvent: function(message) {
					this.$BpCTILog += "● " + message + "\r\n";
					let el = Ext.get("BpCandidateCTIPageV2BpCTILogMemoEdit-el");
					if (el) {
						el.scroll('b', Infinity);
					}
				},

				// Get operator contact role.
				setOperatorRoleId: function() {
					console.log("setOperatorRoleId");
					var contactId = Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Contact"
					});
					esq.addColumn("DecisionRole.Id");
					esq.getEntity(contactId, function(response) {
						if (response.success && response.entity) {
							this.$OperatorRoleId = response.entity.get("DecisionRole.Id");
						}
					}, this);
				},

				// Return true if vacancy selected.
				getOpportunityLinkVisible: function() {
					return !Ext.isEmpty(this.$SxVacancy);
				},

				// Return true if vacancy selected.
				getAccountLinkVisible: function() {
					return !Ext.isEmpty(this.$SxVacancy);
				},

				// Return true if vacancy selected.
				getVacancyLinkVisible: function() {
					return !Ext.isEmpty(this.$SxVacancy);
				},

				// Return true if vacancy selected and candidate saved.
				getCandidateLinkVisible: function() {
					return !Ext.isEmpty(this.$CandidateId) && !this.$IsNewCandidate;
				},

				// Return true if we find existing vacancy candidate or vacancy candidate was saved.
				getVacancyCandidateLinkVisible: function() {
					return this.$IsVacancyCandidateExist || this.$VacancyCandidateSaved;
				},

				// Return true if vacancy selected.
				getGeneralGridVisible: function() {
					return !Ext.isEmpty(this.$SxVacancy);
				},

				onLinkButtonClick: function() {
					var tag = arguments[3];
					var link = window.location.origin + "/0/Nui/ViewModule.aspx#CardModuleV2/";

					switch (tag) {
						case "OpportunityLink":
							link += "OpportunityPageV2/edit/" + this.$SxVacancy.SxOpportunity.value;
							break;
						case "VacancyLink":
							link += "SxVacancyPage/edit/" + this.$SxVacancy.value;
							break;
						case "CandidateLink":
							link += "SxCandidatePage/edit/" + this.$CandidateId;
							break;
						case "AccountLink":
							link += "AccountPageV2/edit/" + this.$SxVacancy["SxOpportunity.Account"].value;
							break;
						case "VacancyCandidateLink":
							var vacancyCandidateId = this.$VacancyCandidateSaved ? this.$Id : this.$VacancyCandidateId;
							link += "SxVacancyCandidatePage/edit/" + vacancyCandidateId;
							break;
						default:
							window.console.warn("Link not found.");
							return;
					}

					window.open(link);
				},

				getCreateDialogScenarioPopupButtonVisible: function() {
					return !Ext.isEmpty(this.$SxVacancy);
				},

				// Set default values.
				setDefValues: function() {
					// Set 'Responce' call source if outgoing call, set 'Income' if incoming call.
					let callDirection = this.get("CallType");
					this.loadLookupDisplayValue("BpCallSourceType",
						callDirection === "outgoing" ? BpClientConstants.CallSourceType.Response : BpClientConstants.CallSourceType.Income);

					this.loadLookupDisplayValue("BpHandleType", BpClientConstants.HandleType.Operator);
					this.$BpStatusDetails = null;
					this.set("SxNotes", "");
					this.getOpportunityInformation();
				},

				// Set call source by phone in advertising.
				setCallSourceByInitNumber: function() {
					console.log("setCallSourceByInitNumber");
					let initNumber = this.get("initNumber");
					if (!Ext.isEmpty(initNumber)) {
						let esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "SxAdPhone"
						});
						esq.addColumn("BpCallSource");
						esq.filters.add("filterAdPhoneByPhoneNumber", esq.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.CONTAIN, "SxPhone", initNumber));
						esq.filters.add("filterBpCallSourceNotNull", Terrasoft.createColumnIsNotNullFilter("BpCallSource"));
						esq.getEntityCollection(function(result) {
							if (result.success) {
								if (result.collection.getCount() >= 1) {
									let firstItem = result.collection.getByIndex(0);

									this.loadLookupDisplayValue("BpCallSource", firstItem.values.BpCallSource.value);
									this.logEvent("Для данного номера найден источник звонка.");
								}
							}
						}, this);
					}
				},

				// Set opportunity and vacancy by phone in advertising.
				// Set only opportunity, if several vacancies was found.
				setOpportunityAndVacancyByInitNumber: function() {
					console.log("setOpportunityAndVacancyByInitNumber");
					let initNumber = this.get("initNumber");
					if (!Ext.isEmpty(initNumber)) {
						let esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "SxOpportunityAdPhone"
						});
						esq.addColumn("SxVacancy");
						esq.addColumn("SxOpportunity");
						esq.filters.add("filterVacancyByPhoneNumber", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.CONTAIN, "SxNumber", initNumber));
						esq.filters.add("filterVacancyByStatus", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "SxVacancy.SxVacancyStage", BpClientConstants.Vacancy.Stage.InWork));
						esq.getEntityCollection(function(result) {
							if (result.success) {
								console.log("setOpportunityAndVacancyByInitNumber", result);
								if (result.collection.getCount() > 0) {
									let firstItem = result.collection.getByIndex(0);
									let opportunityId = firstItem.values.SxOpportunity.value;

									if (result.collection.getCount() === 1) {
										let vacancyId = firstItem.values.SxVacancy.value;
										this.loadLookupDisplayValue("SxVacancy", vacancyId);
										this.logEvent("Для данного номера найдена одна вакансия. Она будет выбрана по умолчанию.");
									}
									else {
										this.logEvent("Для данного номера найдены несколько вакансий.");
										this.setOpportunity(opportunityId);
									}
								}
							}
						}, this);
					}
				},

				// On status in dashboard changed.
				onStatusChanging: function() {
					this.$BpStatusDetails = null;
					this.setIsStatusDetailsRequired();
					this.setIsStatusDetailsVisible();
				},

				// Set status details require.
				setIsStatusDetailsRequired: function() {
					if (!this.$BpStatus) {
						return;
					}

					this.$IsStatusDetailsRequired = this.$BpStatus.BpIsDetailsRequired;
				},

				// Show status details if it exist.
				setIsStatusDetailsVisible: function() {
					if (!this.$BpStatus) {
						return;
					}

					let esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "BpCandidateStatusDetails"
					});
					esq.filters.add("detailsExistFilter", esq.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL,
						"[BpCandidateStatusToDetails:BpCandidateStatusDetails].BpCandidateStatus.Id",
						this.$BpStatus.value));

					esq.getEntityCollection(function (response) {
						if (response.success && response.collection.collection.length > 0) {
							this.$IsStatusDetailsVisible = true;
						} else {
							this.$IsStatusDetailsVisible = false;
						}
					}, this);
				},

				// On vacancy change.
				onVacancyChanging: function() {
					this.setDefValues();

					this.loadLookupDisplayValue("BpStatus", BpClientConstants.VacancyCandidate.Status.Unknown);

					if (!this.$SxVacancy) {
						this.$IsVacancyRequirementsVisible = false;
						this.$IsVacancyRequirementsV2Visible = false;
						this.$IsVacancyCandidateExist = false;
						return;
					}

					this.logEvent("Вакансия была изменена.");

					this.$AgeFrom = this.$SxVacancy.SxAgeFrom;
					this.$AgeTill = this.$SxVacancy.SxAgeTill;

					this.$IsVacancyRequirementsVisible = !this.$SxVacancy["SxOpportunity.BpUseNewRequirement"];
					this.$IsVacancyRequirementsV2Visible = this.$SxVacancy["SxOpportunity.BpUseNewRequirement"];

					this.updateDetails();

					this.setNextStage();
					
					// Send message to cti panel.
					this.sandbox.publish("VacancySelected", {
						speech: this.$SxVacancy.SxCallScript,
						opportunity: this.$SxVacancy.SxOpportunity.value,
						vacancy: this.$SxVacancy.value
					}, []);

					this.updateCandidateParametersModule();
					this.sandbox.publish("SetFilterModuleConfig", this.getRequirementsModuleConfig(), [this.modules.FilterModule.moduleId]);

					Terrasoft.chain(
						// Try to find vacancy-candidate pair.
						function isVacancyCandidateExist(next) {
							var candidateId = this.$CandidateId;
							var vacancyId = this.$SxVacancy.value;
							var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "SxVacancyCandidate"
							});
							esq.filters.add("vacancyFilter", Terrasoft.createColumnFilterWithParameter(
								Terrasoft.ComparisonType.EQUAL, "SxVacancy", vacancyId
							));
							esq.filters.add("candidateFilter", Terrasoft.createColumnFilterWithParameter(
								Terrasoft.ComparisonType.EQUAL, "SxCandidate", candidateId
							));
							esq.rowCount = 1;

							esq.getEntityCollection(function(result) {
								if (result.success && result.collection.getCount() === 1) {
									let firstItem = result.collection.getByIndex(0);

									this.$IsVacancyCandidateExist = true;
									this.$VacancyCandidateId = firstItem.values.Id;

									this.logEvent("Для пары Кандидат-Вакансия найдено КВВ.");
								} else {
									this.$IsVacancyCandidateExist = false;
									this.$VacancyCandidateId = "";
								}

								next();
							}, this);
						},
						// Continue if vacancy-candidate not exist.
						// Or open vacancy-candidate page or clear vacancy value.
						function() {
							if (this.$IsVacancyCandidateExist) {
								this.showExistVacancyCandidateConfirmationDialog();
								return;
							}
						},
						this
					);

					// Update call.
					if (!Ext.isEmpty(this.get("callUniqueId"))) {
						let curCall = this.get("callUniqueId");
						let update = this.Ext.create("Terrasoft.UpdateQuery", {
							rootSchemaName: "Call"
						});
						update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "IntegrationId", curCall));
						update.setParameterValue("Opportunity", this.$SxVacancy.SxOpportunity.value, this.Terrasoft.DataValueType.GUID);
						update.setParameterValue("SxVacancy", this.$SxVacancy.value, this.Terrasoft.DataValueType.GUID);
						update.execute(function(response) {
							if (response.success) {
								this.logEvent("Звонок обновлен. Добавлены Проект и Вакансия.");
							}
						}, this);
					}
				},

				// Show open existing vacancy candidate dialog.
				showExistVacancyCandidateConfirmationDialog: function() {
					var buttonOpen = {
						"className": "Terrasoft.Button",
						"returnCode": "open",
						"style": "green",
						"caption": "Открыть КВВ"
					};
					this.showConfirmationDialog(
						"Для пары Кандидат-Вакансия найдено КВВ.\nЧтобы продолжить, откройте карточку КВВ.",
						function getSelectedButton(returnCode) {
							if (returnCode === "open") {
								var moduleName = "CardModuleV2/SxVacancyCandidatePage";
                                moduleName += "/edit/"+ this.$VacancyCandidateId;
                                this.sandbox.publish("PushHistoryState", { hash: moduleName });
							}
						},
						["no", buttonOpen],
						{style: Terrasoft.MessageBoxStyles.BLUE},
						{defaultButton: 1}
					);
				},

				// Build info labels.
				getOpportunityInformation: function() {
					if (!this.$SxVacancy) {
						this.$AccountOpportunityVacancyCaption = "";
						this.$DaysBeforeClosingOpportunity = "";
						return;
					}

					let accountOpportunityVacancyCaption = [];

					// Account name.
					accountOpportunityVacancyCaption.push(this.$SxVacancy["SxOpportunity.Account.Name"]);

					// Opportunity title.
					accountOpportunityVacancyCaption.push(this.$SxVacancy["SxOpportunity.Title"]);

					// Vacancy title.
					accountOpportunityVacancyCaption.push(this.$SxVacancy.displayValue);

					this.$AccountOpportunityVacancyCaption = accountOpportunityVacancyCaption.join(" - ");

					// Days before closing opportunity.
					let currentDate = new Date();
					let planningEndDate = this.$SxVacancy["SxOpportunity.SxPlanningEndDate"];
					let differenceInDays = Math.round((planningEndDate - currentDate) / 86400000);
					this.$DaysBeforeClosingOpportunity = Ext.String.format(
						this.get("Resources.Strings.DaysBeforeClosingOpportunity"), differenceInDays);
				},

				// Get first stage by vacancy.
				setNextStage: function() {
					let vacancyId = this.$SxVacancy.value;
					let esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "BpCandidateStage"
					});
					esq.addColumn("Id");
					let rankCol = esq.addColumn("BpRank");
					rankCol.orderDirection = Terrasoft.OrderDirection.ASC;
					rankCol.orderType = 1;
					esq.filters.add("rankFilter", esq.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.GREATER, "BpRank", 0));
					esq.filters.add("vacancyFilter", esq.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL,
						"[BpServiceCandidateStage:BpCandidateStage].BpService.[Opportunity:SxService].[SxVacancy:SxOpportunity].Id",
						vacancyId));
					esq.getEntityCollection(function(result) {
						if (result.success) {
							if (result.collection.getCount() > 0) {
								let firstItem = result.collection.getByIndex(0);
								this.loadLookupDisplayValue("BpStage", firstItem.values.Id);
							} else {
								this.$BpStage = undefined;
							}
						}
					}, this);
				},
				
				// Open new vacancy scenario popup.
				onCreateDialogScenarioPopupButtonClick: function() {
					// Получение контейнера, в который будет загружен модуль.
					var currentUserContainer = this.Ext.getCmp("bp-dialog-scenarios-container");
					// Проверка существования контейнера и модуля.
					if (currentUserContainer && currentUserContainer.rendered) {
						var opportunityId = this.$SxVacancy.SxOpportunity.value;
						var vacancyId = this.$SxVacancy.value;
						
						var parameters = {
							Opportunity: opportunityId,
							Vacancy: vacancyId
						};
						
						this.sandbox.publish("CreateDialogScenarioPopup", parameters);
					}
				},

				// On workplace address selected.
				onWorkplaceAddressChanged: function() {
					this.setWorkplaceDisplayValue();
				},

				// Set Workplace column text value and WorkplaceAddress lookup display value.
				setWorkplaceDisplayValue: function() {
					var workplaceAddress = this.$SxWorkplaceAddress;
					if (!workplaceAddress) {
						return;
					}

					var workplaceAddressDisplayValue = this.getDisplayFullAddress(workplaceAddress);
					this.$SxWorkplace = workplaceAddressDisplayValue;
					workplaceAddress.displayValue = workplaceAddressDisplayValue;

					var workplaceAddressCmp = Ext.getCmp("workplace-address-edit");
					workplaceAddressCmp.setValue(workplaceAddress);
					workplaceAddressCmp.safeRerender();
				},

				// On interview address selected.
				onInterviewAddressChanged: function() {
					this.setInterviewPlaceDisplayValue();
				},

				// Set Interview column text value and InterviewPlace lookup display value.
				setInterviewPlaceDisplayValue: function() {
					var interviewAddress = this.$BpInterviewAddress;
					if (!interviewAddress) {
						return;
					}

					var interviewAddressDisplayValue = this.getDisplayFullAddress(interviewAddress);
					this.$BpInterviewPlace = interviewAddressDisplayValue;
					interviewAddress.displayValue = interviewAddressDisplayValue;

					var interviewAddressCmp = Ext.getCmp("interview-address-edit");
					interviewAddressCmp.setValue(interviewAddress);
					interviewAddressCmp.safeRerender();
				},

				// Build full address string from AccountAddress column.
				getDisplayFullAddress: function(addressColumnValue) {
					var fullAddress = [];

					var city = addressColumnValue["City.Name"].trim();
					var address = addressColumnValue.Address.trim();
					var subway = addressColumnValue["SxSubwayStation.Name"].trim();

					if (city) {
						fullAddress.push(city);
					}

					if (address) {
						fullAddress.push(address);
					}

					if (subway) {
						fullAddress.push(subway);
					}

					return fullAddress.join(", ");
				},

				// Get call if it exist. Timeout and 15 attempts, if call is not exist.
				tryUpdateVacancyCandidateInCall: function(vacancyCandidateId, currCallUniqueId, attemptNum) {
					if (!currCallUniqueId) {
						return;
					}

					let esqId = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Call"
					});
					esqId.addColumn("Id");

					esqId.filters.add("CallByUniqueId", esqId.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "IntegrationId", currCallUniqueId));
					esqId.getEntityCollection(function (response) {
						if (response.success) {
							if (response.collection.getCount() === 1) {
								let firstItem = response.collection.getByIndex(0);
								let callActualId = firstItem.values.Id;

								this.updateVacancyCandidateInCall(vacancyCandidateId, callActualId);
							}
							else {
								attemptNum++;
								if (attemptNum < 15) {
									setTimeout(this.tryUpdateVacancyCandidateInCall.bind(this), 100, vacancyCandidateId, currCallUniqueId, attemptNum);
								}
								else {
									return;
								}
							}
						}
					}, this);
				},

				// Update existing call.
				updateVacancyCandidateInCall: function(vacancyCandidateId, callActualId) {
					if (Ext.isEmpty(vacancyCandidateId) || Ext.isEmpty(callActualId)) {
						return;
					}

					let update = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "Call"
					});

					update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", callActualId));
					update.setParameterValue("BpVacancyCandidate", vacancyCandidateId, this.Terrasoft.DataValueType.GUID);
					update.execute(function(response) {
						if (response.success) {
							this.logEvent("Звонок обновлен. Добавлено КВВ.");
						}
					}, this);

					let updateVC = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "SxVacancyCandidate"
					});

					updateVC.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", vacancyCandidateId));
					updateVC.setParameterValue("BpInitCall", callActualId, this.Terrasoft.DataValueType.GUID);
					updateVC.execute();
				},

				updateCandidateParametersModule: function() {
					this.sandbox.publish("UpdateCandidateParametersModule", null, []);
				},

				// Readonly filter module config.
				getRequirementsModuleConfig: function() {
					return {
						rootSchemaName: "SxCandidate",
						filters: this.$SxVacancy ? this.$SxVacancy.BpRequirementFilterValue : "",
						actionsVisible: false,
						entitySchemaFilterProviderConfig: {
							canDisplayId: true,
							shouldHideLookupActions: true,
							isColumnSettingsHidden: true,
							getAllowedFilterManageOperations: function() {
								return {
									canViewEnabled: false,
									canEditEnabled: false,
									canEditLeftExpression: false,
									canEditRightExpression: false,
									canEditComparisonType: false,
									canRemove: false,
									canSelect: false
								};
							},
							getAllowedFilterGroupManageOperations: function() {
								return {
									canViewEnabled: false,
									canEditEnabled: false,
									canViewGroupType: true,
									canEditGroupType: false,
									canAddFilters: false,
									canRemove: false,
									canSelect: false
								};
							}
						}
					};
				},
			},
			
			diff: [
				// Actions dashboard.
				{
					"operation": "insert",
					"name": "ActionsDashboardModule",
					"parentName": "ActionDashboardContainer",
					"propertyName": "items",
					"values": {
						"classes": { wrapClassName: ["actions-dashboard-module"] },
						"itemType": Terrasoft.ViewItemType.MODULE
					}
				},

				//#region Header
				// "Account name - Opportuniyu name - Vacancy name" label.
				{
					"operation": "insert",
					"name": "AccountOpportunityVacancyLabel",
					"values": {
						"itemType": Terrasoft.ViewItemType.LABEL,
						"caption": { "bindTo": "AccountOpportunityVacancyCaption" },
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 0
				},
				// Opportunity link.
				{
					"operation": "insert",
					"name": "OpportunityHyperlink",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"caption": { "bindTo": "Resources.Strings.OpportunityLinkCaption" },
						"click": { "bindTo": "onLinkButtonClick" },
						"tag": "OpportunityLink",
						"layout": {
							"column": 12,
							"row": 0,
							"colSpan": 6
						},
						"visible": { "bindTo": "getOpportunityLinkVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 1
				},
				// Days before opportunity closing label.
				{
					"operation": "insert",
					"name": "BeforeClosingOpportunityLabel",
					"values": {
						"itemType": Terrasoft.ViewItemType.LABEL,
						"caption": { "bindTo": "DaysBeforeClosingOpportunity" },
						"layout": {
							"column": 0,
							"row": 1,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 2
				},
				// Vacancy link.
				{
					"operation": "insert",
					"name": "VacancyHyperlink",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"caption": { "bindTo": "Resources.Strings.VacancyLinkCaption" },
						"click": { "bindTo": "onLinkButtonClick" },
						"tag": "VacancyLink",
						"layout": {
							"column": 12,
							"row": 1,
							"colSpan": 6
						},
						"visible": { "bindTo": "getVacancyLinkVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 3
				},
				// Account link.
				{
					"operation": "insert",
					"name": "AccountHyperLink",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"caption": { "bindTo": "Resources.Strings.AccountLinkCaption" },
						"click": { "bindTo": "onLinkButtonClick" },
						"tag": "AccountLink",
						"layout": {
							"column": 12,
							"row": 2,
							"colSpan": 6
						},
						"visible": { "bindTo": "getAccountLinkVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 4
				},
				// Candidate link.
				{
					"operation": "insert",
					"name": "CandidateHyperLink",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"caption": { "bindTo": "Resources.Strings.CandidateLinkCaption" },
						"click": { "bindTo": "onLinkButtonClick" },
						"tag": "CandidateLink",
						"layout": {
							"column": 12,
							"row": 3,
							"colSpan": 6
						},
						"visible": { "bindTo": "getCandidateLinkVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 5
				},
				// Candidate last name.
				{
					"operation": "insert",
					"name": "LastName",
					"values": {
						"bindTo": "LastName",
						"caption": { "bindTo": "Resources.Strings.LastNameCaption" },
						"layout": {
							"column": 0,
							"row": 4,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 6
				},
				// Vacancy candidate warning and link.
				{
					"operation": "insert",
					"name": "VacancyCandidateLink",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"caption": { "bindTo": "Resources.Strings.VacancyCandidateLinkCaption" },
						"click": { "bindTo": "onLinkButtonClick" },
						"tag": "VacancyCandidateLink",
						"layout": {
							"column": 12,
							"row": 4,
							"colSpan": 6
						},
						"visible": { "bindTo": "getVacancyCandidateLinkVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 7
				},
				// BpCTILog.
				{
					"operation": "insert",
					"name": "BpCTILog",
					"values": {
						"contentType": this.Terrasoft.ContentType.LONG_TEXT,
						"bindTo": "BpCTILog",
						"labelConfig": {
							"visible": false
						},
						"controlConfig": {
							"height": "150px"
						},
						"layout": {
							"column": 18,
							"row": 0,
							"colSpan": 6,
							"rowSpan": 5
						},
						"enabled": false
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 8
				},
				// Candidate first name.
				{
					"operation": "insert",
					"name": "FirstName",
					"values": {
						"bindTo": "FirstName",
						"caption": { "bindTo": "Resources.Strings.FirstNameCaption" },
						"layout": {
							"column": 0,
							"row": 5,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 9
				},
				// Candidate middle name.
				{
					"operation": "insert",
					"name": "MiddleName",
					"values": {
						"bindTo": "MiddleName",
						"caption": { "bindTo": "Resources.Strings.MiddleNameCaption" },
						"layout": {
							"column": 0,
							"row": 6,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 10
				},
				// Operator phone number.
				{
					"operation": "insert",
					"name": "OperatorPhoneNumber",
					"values": {
						"bindTo": "OperatorPhoneNumber",
						"caption": { "bindTo": "Resources.Strings.OperatorPhoneNumberCaption" },
						"layout": {
							"column": 12,
							"row": 6,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 11
				},
				// Candidate mobile phone.
				{
					"operation": "insert",
					"name": "MobilePhone",
					"values": {
						"bindTo": "MobilePhone",
						"caption": { "bindTo": "Resources.Strings.MobilePhoneCaption" },
						"layout": {
							"column": 0,
							"row": 7,
							"colSpan": 12
						},
						"enabled": false
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 12
				},
				// Candidate email.
				{
					"operation": "insert",
					"name": "Email",
					"values": {
						"bindTo": "Email",
						"caption": { "bindTo": "Resources.Strings.EmailCaption" },
						"layout": {
							"column": 12,
							"row": 7,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 13
				},
				// Call source.
				{
					"operation": "insert",
					"name": "BpCallSource",
					"values": {
						"dataValueType": Terrasoft.DataValueType.LOOKUP,
						"bindTo": "BpCallSource",
						"caption": "Источник звонка",
						"layout": {
							"column": 0,
							"row": 8,
							"colSpan": 7
						},
						"isRequired": true
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 14
				},
				// Call source type.
				{
					"operation": "insert",
					"name": "BpCallSourceType",
					"values": {
						"contentType": Terrasoft.ContentType.ENUM,
						"bindTo": "BpCallSourceType",
						"labelConfig": {
							"visible": false
						},
						"layout": {
							"column": 7,
							"row": 8,
							"colSpan": 3
						},
						"isRequired": true
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 15
				},
				// Handle type.
				{
					"operation": "insert",
					"name": "BpHandleType",
					"values": {
						"contentType": Terrasoft.ContentType.ENUM,
						"bindTo": "BpHandleType",
						"labelConfig": {
							"visible": false
						},
						"layout": {
							"column": 10,
							"row": 8,
							"colSpan": 2
						},
						"enabled": false,
						"isRequired": true
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 16
				},
				// SxVacancy.
				{
					"operation": "insert",
					"name": "SxVacancy",
					"values": {
						"dataValueType": Terrasoft.DataValueType.LOOKUP,
						"bindTo": "SxVacancy",
						"layout": {
							"column": 12,
							"row": 8,
							"colSpan": 9
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 17
				},
				// Open vacancy dialog scenario in popup.
				{
					"operation": "insert",
					"name": "CreateDialogScenarioPopupButton",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.GREY,
						"click": {"bindTo": "onCreateDialogScenarioPopupButtonClick"},
						"caption": { "bindTo": "Resources.Strings.CreateDialogScenarioPopupButtonCaption" },
						"layout": {
							"column": 21,
							"row": 8,
							"colSpan": 3
						},
						"visible": { "bindTo": "getCreateDialogScenarioPopupButtonVisible" }
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 18
				},
				// Candidate description.
				{
					"operation": "insert",
					"name": "Description",
					"values": {
						"bindTo": "Description",
						"caption": { "bindTo": "Resources.Strings.DescriptionCaption" },
						"layout": {
							"column": 0,
							"row": 9,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 19
				},
				// Candidate career objective.
				{
					"operation": "insert",
					"name": "CareerObjective",
					"values": {
						"bindTo": "CareerObjective",
						"caption": { "bindTo": "Resources.Strings.CareerObjectiveCaption" },
						"layout": {
							"column": 12,
							"row": 9,
							"colSpan": 12
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 20
				},
				// Comments.
				{
					"operation": "insert",
					"name": "BpComments",
					"values": {
						"bindTo": "BpComments",
						"layout": {
							"column": 0,
							"row": 10,
							"colSpan": 24
						}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 21
				},
				//#endregion Header

				// Remove ESN tab.
				{
					"operation": "remove",
					"name": "ESNTab"
				},

				//#region General info tab
				{
					"operation": "insert",
					"name": "GeneralInfoTab",
					"values": {
						"caption": { "bindTo": "Resources.Strings.GeneralInfoTabCaption" },
						"items": []
					},
					"parentName": "Tabs",
					"propertyName": "tabs",
					"index": 0,
				},
				// Requirements old.
				{
					"operation": "insert",
					"name": "RequirementsGroup",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"items": [],
						"caption": { "bindTo": "Resources.Strings.RequirementsGroupCaption" },
						"visible": {"bindTo": "IsVacancyRequirementsVisible"}
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 0
				},

				//#region Requirements old
				// Requirements grid layout.
				{
					"operation": "insert",
					"name": "RequirementsGridLayout",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": []
					},
					"propertyName": "items",
					"parentName": "RequirementsGroup",
					"index": 0
				},
				// Age from.
				{
					"operation": "insert",
					"name": "AgeFrom",
					"values": {
						"bindTo": "AgeFrom",
						"caption": { "bindTo": "Resources.Strings.AgeFromCaption" },
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 6
						},
					},
					"parentName": "RequirementsGridLayout",
					"propertyName": "items",
					"index": 0
				},
				// Age till.
				{
					"operation": "insert",
					"name": "AgeTill",
					"values": {
						"bindTo": "AgeTill",
						"caption": { "bindTo": "Resources.Strings.AgeTillCaption" },
						"layout": {
							"column": 6,
							"row": 0,
							"colSpan": 6
						},
					},
					"parentName": "RequirementsGridLayout",
					"propertyName": "items",
					"index": 1
				},
				// Vacancy requirements detail.
				{
					"operation": "insert",
					"name": "VacancyRequirement",
					"values": {
						"itemType": Terrasoft.ViewItemType.DETAIL
					},
					"parentName": "RequirementsGroup",
					"propertyName": "items",
					"index": 2
				},
				//#endregion Requirements old

				// Requirements V2.
				{
					"operation": "insert",
					"name": "RequirementsV2Group",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"items": [],
						"caption": { "bindTo": "Resources.Strings.RequirementsV2GroupCaption" },
						"visible": {"bindTo": "IsVacancyRequirementsV2Visible"}
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 1
				},
				// Filter module
				{
					"operation": "insert",
					"name": "FilterModule",
					"values": {
						"id": "FilterModule",
						"itemType": Terrasoft.ViewItemType.MODULE,
						"items": []
					},
					"parentName": "RequirementsV2Group",
					"propertyName": "items",
					"index": 0
				},

				// Candidate parameters module container.
				{
					"operation": "insert",
					"name": "CandidateParametersContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 2
				},
				// Candidate parameters module.
				{
					"operation": "insert",
					"name": "CandidateParametersModule",
					"values": {
						"itemType": Terrasoft.ViewItemType.MODULE,
						"items": [],
						"visible": {
							"bindTo": "IsNewCandidate",
							"bindConfig": {
								converter: function(value) {
									return !value;
								}
							}
						}
					},
					"parentName": "CandidateParametersContainer",
					"propertyName": "items",
					"index": 0
				},

				//#region General grid layout
				// General grid layout.
				{
					"operation": "insert",
					"name": "GeneralGridLayout",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"visible": {"bindTo": "getGeneralGridVisible"},
						"items": []
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 3
				},
				// Status details.
				{
					"operation": "insert",
					"name": "BpStatusDetails",
					"values": {
						"dataValueType": Terrasoft.DataValueType.ENUM,
						"bindTo": "BpStatusDetails",
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12,
						},
						"visible": {"bindTo": "IsStatusDetailsVisible"},
						"isRequired": {"bindTo": "IsStatusDetailsRequired"}
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 0
				},
				// Failure statistics detail
				{
					"operation": "insert",
					"name": "BpVacancyCandidateFailureStatisticsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail",
						"layout": {
							"column": 12,
							"row": 0,
							"colSpan": 12,
						},
					//	"visible": {"bindTo": "IsStatusDetailsVisible"}
						"visible": false
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 2
				},
				// Workplace address.
				{
					"operation": "insert",
					"name": "SxWorkplaceAddress",
					"values": {
						"id": "workplace-address-edit",
						"contentType": Terrasoft.ContentType.LOOKUP,
						"bindTo": "SxWorkplaceAddress",
						"layout": {
							"colSpan": 12,
							"column": 0,
							"row": 1
						}
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 3
				},
				// Interview address.
				{
					"operation": "insert",
					"name": "BpInterviewAddress",
					"values": {
						"id": "interview-address-edit",
						"dataValueType": Terrasoft.DataValueType.LOOKUP,
						"bindTo": "BpInterviewAddress",
						"layout": {
							"colSpan": 12,
							"column": 12,
							"row": 1
						}
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 4
				},
				// Store.
				{
					"operation": "insert",
					"name": "BpStore",
					"values": {
						"dataValueType": Terrasoft.DataValueType.LOOKUP,
						"bindTo": "BpStore",
						"layout": {
							"column": 0,
							"row": 2,
							"colSpan": 12
						}
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 5
				},
				// Note.
				{
					"operation": "insert",
					"name": "SxNotes",
					"values": {
						"contentType": this.Terrasoft.ContentType.LONG_TEXT,
						"bindTo": "SxNotes",
						"layout": {
							"column": 0,
							"row": 3,
							"colSpan": 24,
							"rowSpan": 3
						},
						"isRequired": { "bindTo": "isNoteRequired" }
					},
					"parentName": "GeneralGridLayout",
					"propertyName": "items",
					"index": 6
				},
				//#endregion General grid layout

				// Candidate addresses detail.
				{
					"operation": "insert",
					"name": "CandidateAddress",
					"values": {
						"itemType": Terrasoft.ViewItemType.DETAIL,
						"visible": {
							"bindTo": "IsNewCandidate",
							"bindConfig": {
								converter: function(value) {
									return !value;
								}
							}
						}
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 4
				},
				//#endregion General info tab
			],
			
			rules: {}
		};
});