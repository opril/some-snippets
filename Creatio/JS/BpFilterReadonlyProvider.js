define("BpFilterReadonlyProvider", [],
	function() {
		/**
		 * @class Terrasoft.data.filters.FilterReadonlyProvider
		 * Portal entity schema start filters provider.
		 */
		Ext.define("Terrasoft.data.filters.FilterReadonlyProvider", {
			extend: "Terrasoft.BaseFilterProvider",
			alternateClassName: "Terrasoft.FilterReadonlyProvider",
			
			rootSchemaName: "",
			
			canDisplayId: false,
			
			sandbox: null,
			
			// Управление операциями внутри фильтра (для каждого фильтра по отдельности).
			getAllowedFilterManageOperations: function() {
				return {
					// Видимость признака "Активен".
					canViewEnabled: true,
					// Возможность изменения признака "Активен".
					canEditEnabled: false,
					// Возможность изменения левой части выражения.
					canEditLeftExpression: false,
					// Возможность изменения правой части выражения.
					canEditRightExpression: false,
					// Возможность изменения условия сравнения.
					canEditComparisonType: false,
					// Видимость кнопки удаления ('X').
					canRemove: false,
					// Возможность выбрать отдельный фильтр.
					canSelect: false
				};
			},
			
			// Управление группами фильтров.
			getAllowedFilterGroupManageOperations: function() {
				return {
					// Видимость признака "Активен".
					canViewEnabled: true,
					// Возможность изменения признака "Активен".
					canEditEnabled: false,
					// Видимость типа группы ("И" / "ИЛИ").
					canViewGroupType: true,
					// Возможность редактирования типа группы.
					canEditGroupType: false,
					// Видимость кнопки "Добавить условие".
					canAddFilters: false,
					// Видимость кнопки удаления ('X').
					canRemove: false,
					// Возможность выбора группы фильтра.
					canSelect: false
				};
			}

		});
		return Terrasoft.FilterReadonlyProvider;
	}
);
